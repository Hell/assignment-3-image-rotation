#include "bmp.h"


#define BYTE_ALIGNMENT 4
#define BMP_TYPE 0x4D42
#define BMP_STRUCT_SIZE sizeof(struct bmp_header)
#define BI_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0
#define BI_X_PELS_PER_METER 2835
#define BI_Y_PELS_PER_METER 2835
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0


uint64_t calc_padding(uint64_t width) {
    return BYTE_ALIGNMENT - (width * sizeof(struct pixel) % BYTE_ALIGNMENT) % BYTE_ALIGNMENT;
}


uint64_t calc_size(const struct image* img) {
    return (img->width * sizeof(struct pixel) + calc_padding(img->width)) * img->height;
}


struct bmp_header get_header(const struct image* img) {
    return (struct bmp_header) {
            .bfType = BMP_TYPE,
            .bfileSize = BMP_STRUCT_SIZE + calc_size(img),
            .bfReserved = BI_RESERVED,
            .bOffBits = BMP_STRUCT_SIZE,
            .biSize = BI_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BI_PLANES,
            .biBitCount = BI_BIT_COUNT,
            .biCompression = BI_COMPRESSION,
            .biSizeImage = calc_size(img),
            .biXPelsPerMeter = BI_X_PELS_PER_METER,
            .biYPelsPerMeter = BI_Y_PELS_PER_METER,
            .biClrUsed = BI_CLR_USED,
            .biClrImportant = BI_CLR_IMPORTANT
    };
}


enum bmp_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;
    if (!fread(&header, sizeof(struct bmp_header), 1, in) || header.bfType != BMP_TYPE)
        return BMP_INVALID_HEADER;
    
    if (header.biBitCount != BI_BIT_COUNT)
        return BMP_INVALID_BIT_COUNT;
    
    *img = image_create(header.biWidth, header.biHeight);
    uint8_t padd = calc_padding(img->width);
    
    if (!img->data)
        return BMP_INVALID_MEMORY_ALLOCATION;

    for (uint64_t h = 0; h < img->height; h++) {
        if (fread(img->data + img->width * h, sizeof(struct pixel), img->width, in) != img->width)
            return BMP_INVALID_BITS;
        if (fseek(in, padd, SEEK_CUR))
            return BMP_INVALID_SIGNATURE;
    }
    return BMP_OK;
}


enum bmp_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = get_header(img);
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out) || header.bfType != BMP_TYPE)
        return BMP_INVALID_HEADER;

    uint64_t padd = calc_padding(img->width);
    uint64_t padding_value = 0;

    for (uint64_t h = 0; h < img->height; h++) {
        if (!fwrite(img->data + h * img->width, sizeof(struct pixel), img->width, out))
            return BMP_INVALID_BITS;
        if (!fwrite(&padding_value, sizeof(uint8_t), padd, out))
            return BMP_INVALID_PADDING;
    }

    return BMP_OK;
}
