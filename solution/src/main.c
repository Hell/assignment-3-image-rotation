#include "main.h"

int main( int argc, char** argv ) {
    struct image img = {0};
    FILE *in = NULL;
    FILE *out = NULL; 
    if (argc != 4)
    {
        error("Format: <source-image> <transformed-image> <angle>", &img, &in, &out);
        return 1;
    }
    in = fopen(argv[1], "rb");
    out = fopen(argv[2], "wb");
    
    int angle = atoi(argv[3]) % 360;
    if (angle < 0)
        angle += 360;
    if (angle % 90 != 0) {
        error("Invalid angle, allowed angles: 0, 90, -90, 180, -180, 270, -270", &img, &in, &out);
        return 2;
    }

    if (!in)
    {
        error("Invalid input file!", &img, &in, &out);
        return 3;
    }
    if (!out) {
        error("Invalid output file!", &img, &in, &out);
        return 4;
    }  

    enum bmp_status status = from_bmp(in, &img);
    if (status != BMP_OK) {
        status_error(status, "read file", &img, &in, &out);
        return 5;
    }

    struct image res = img;
    for (int i = 0; i < angle/90; i++) {
        struct image temp = rotate(res);
        image_free(&res);
        res = temp;
    }

    enum bmp_status statusw = to_bmp(out, &res);
    if (statusw != BMP_OK)
    {
        status_error(statusw, "result file", &res, &in, &out);
        return 6;
    }

    if (fclose(in))
    {
        error("Failed to close read file!", &res, &in, &out);
        return 7;
    }

    if (fclose(out))
    {
        error("Failed to close result!", &res, &in, &out);
        return 8;
    }

    image_free(&res);
    return 0;
}
