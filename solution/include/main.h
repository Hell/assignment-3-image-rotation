#include "bmp.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>


void error(const char *s, struct image* img, FILE **in, FILE **out) {
    fprintf(stderr, "%s\n", s);
    if (img)
        image_free(img);
    if (in)
        fclose(*in);
    if (out)
        fclose(*out);
    abort();
}
void error2(const char *s1, const char *s2) {
    fprintf(stderr, "%s %s\n", s1, s2);
    abort();
}


void status_error(enum bmp_status status, const char *st, struct image* img, FILE **in, FILE **out) {
    if (img)
        image_free(img);
    if (in)
        fclose(*in);
    if (out)
        fclose(*out);
    switch (status) {
        case BMP_INVALID_SIGNATURE:
            error2("Invalid SIGNATURE of ", st);
            break;
        case BMP_INVALID_BITS:
            error2("Invalid BITS of ", st);
            break;
        case BMP_INVALID_HEADER:
            error2("Invalid HEADER of ", st);
            break;
        case BMP_INVALID_MEMORY_ALLOCATION:
            error2("Invalid MEMORY ALLOCATION of ", st);
            break;
        case BMP_INVALID_BIT_COUNT:
            error2("Invalid BIT COUNT of ", st);
            break;
        case BMP_INVALID_PADDING:
            error2("Invalid PADDING of ", st);
            break;
        default:
            break;
     }
}
